# 1. Define our Base Image
FROM node:latest

# 2. Specify/Create a working directory
WORKDIR /usr/src/app

# 3. Copy Dependencies package.json and package-lock.json docker-node > /usr/src/app
COPY package*.json ./
RUN npm install

# 4. Bundle our source code - Copy it to the WORKDIR
COPY . .

# 5. Expose the PORT of our app to the outside.
EXPOSE 5000

# 6. Run the command to start our app
CMD ["node", "index.js"]