const express = require('express');
const app = express();
const path = require('path');
const emailSentResponse = 'Email has been succesfully sent';
// const router = express.Router();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use( express.static( path.join( __dirname, 'other_pages' ) ) );

//Create a POST Request
app.post('/contact', (req, res) => {
    //req.body holds parameters that are sent up from the client as part of a POST request
    console.log(req.body);
    //Send email to inform of a contact request from website
    // ? What is emailSentResponse?
    return res.status(201).json(emailSentResponse);
});


// app.get('/', (req, res) => res.send('Hello World!'))

app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'other_pages', 'home.html'));
});

app.get('/contact', (req, res) => {
    res.sendFile(path.join(__dirname, 'other_pages', 'contact.html'));
});

app.get('/about', (req, res) => {
    res.sendFile(path.join(__dirname, 'other_pages', 'about.html'));
});

app.get('/menu', (req, res) => {
    res.sendFile(path.join(__dirname, 'other_pages', 'menu.html'));
});

const PORT = process.env.PORT || 5000;
// const HOST = "0.0.0.0";

app.listen(PORT, () => console.log(`App has started on port ${PORT}!`))

// res.sendFile(path.join(__dirname, 'other_pages', 'index.html'));

